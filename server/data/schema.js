import Db from './db'
import Models from './models'
import {
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLInt,
  GraphQLString
} from 'graphql';
import {
  fromGlobalId,
  globalIdField,
  nodeDefinitions,
} from 'graphql-relay';

const {
  nodeField,
  nodeInterface,
} = nodeDefinitions(
  // globalID para objeto
  (globalId, {loaders}) => {
    const {id, type} = fromGlobalId(globalId);
    if (type === 'Message') {
      return loaders.message.load(id);
//      return data[type][id];
    }
  },
  // objeto para type
  (obj) => {
    if (obj.hasOwnProperty('text')) {
      return MessageType;
    }
  }
);

const ThreadType = new GraphQLObjectType({
  name: 'Thread',
  description: 'thread',
  fields: () => ({
    id: globalIdField('Thread'),
    savingMessageJson: {type: GraphQLString},
    messages: {
      type: new GraphQLList(MessageType),
      resolve (obj, args, {loaders}) {
//        console.log(obj);
        return Db.models.message.find({where: args});
      }
    },
  }),
  interfaces: [nodeInterface],
});

const MessageType = new GraphQLObjectType({
  name: 'Message',
  description: 'mensagem',
  fields: () => ({
    id: globalIdField('Message'),
    thread_id: {type: GraphQLInt},
    messageId: {type: GraphQLInt},
    chatId: {type: GraphQLInt},
    messageJson: {type: GraphQLString},
    text: {type: GraphQLString},
    smallPhotoFileName: {type: GraphQLString},
    bigPhotoFileName: {type: GraphQLString},
  }),
  interfaces: [nodeInterface],
});

const QueryType = new GraphQLObjectType({
  name: 'Query',
  description: 'descrição',
  fields: () => ({
    node: nodeField,
    thread: {
      type: ThreadType,
      description: 'Thread',
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
      },
      resolve: (root, args) => Db.models.thread.find({where: args}),
    },
    message: {
      type: MessageType,
      description: 'Mensagem',
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        thread_id: {type: GraphQLInt},
      },
      resolve: (root, args) => Db.models.message.find({where: args}),
    },
  })
});

export default new GraphQLSchema({
  query: QueryType,
});
