import Sequelize from 'sequelize';
import Connection from './db'

const Thread = Connection.define('thread', {
    savingMessageJson: {
      type: Sequelize.STRING,
      allowNull: false,
    }
  }, {
    freezeTableName: true,
    timestamps: false,
});

const Message = Connection.define('message', {
    thread_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    messageId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    chatId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    date: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    messageJson: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    text: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    smallPhotoFileName: {
      type: Sequelize.STRING,
    },
    bigPhotoFileName: {
      type: Sequelize.STRING,
    }
  }, {
    freezeTableName: true,
    timestamps: false
});

Thread.hasMany(Message, {
  foreignKey: 'thread_id'
});
Message.belongsTo(Thread, {
  foreignKey: 'thread_id'
});
/* Testes:
Message.find({
            where: { id: 16 }
        }).then(function(message) {
            console.log(message);
        });
//*/
/*
Thread.find({
            where: { id: 9 }
        }).then(function(thread) {
            console.log(thread);
        });
//*/
